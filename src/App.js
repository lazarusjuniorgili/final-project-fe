import React, { useEffect, useState } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import LandingPage from "./pages/LandingPage";
import ProfilePage from "./pages/ProfilePage";
import ErrorPage from "./pages/ErrorPage";
import Register from "./components/register/register page";
import LeaderBoard from "./components/leaderboard/leaderboardpage";
import Login from "./components/login/login page";
import GamePage from "./pages/GamePage";
import StartPage from "./pages/StartPage";
import Detail from "./components/game/Detail";
import Guide from "./components/game/Guide";
import Hero from "./components/game/Hero";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/style.css";
import AddGame from "./pages/AddGame";

function App() {
  const [authedUser, setAuthedUser] = useState({});
  const navigate = useNavigate();
  const token = localStorage.getItem("accessToken");
  const pathname = window.location.pathname;
  const userId = localStorage.getItem("userId");
  const username = localStorage.getItem("username");

  function generateAuthedUser(userId, username) {
    return { id: userId, username };
  }

  useEffect(() => {
    if (
      !token &&
      pathname !== "/login" &&
      pathname !== "/" &&
      pathname !== "/register"
    )
      navigate("/login");

    if (userId && username) {
      setAuthedUser(() => generateAuthedUser(userId, username));
    }
  }, [navigate, pathname, token, userId, username]);

  function onLogoutHandler() {
    setAuthedUser({});
    localStorage.clear();
    navigate("/");
  }

  return (
    <Routes>
      <Route path="/register" element={<Register />} />
      <Route path="/login" element={<Login />} />
      <Route path="*" element={<ErrorPage />} />
      <Route path="/" element={<LandingPage />} />
      <Route
        path="/profile"
        element={<ProfilePage logout={onLogoutHandler} />}
      />
      <Route path="/game/add" element={<AddGame />} />
      <Route element={<GamePage />}>
        <Route path="/game/:id" element={<Hero />} />
        <Route path="/game/:id/detail" element={<Detail />} />
        <Route path="/game/:id/guide" element={<Guide />} />
      </Route>
      <Route path="/game/:id/start" element={<StartPage />} />
      <Route path="/leaderboard" element={<LeaderBoard />} />
      <Route path="*" element={<ErrorPage authedUser={authedUser} />} />
    </Routes>
  );
}

export default App;
