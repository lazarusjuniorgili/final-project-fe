import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../redux/auth/reducer";
import startReducer from "../redux/start/reducer";
import gamesReducer from "../redux/games/reducer";
import historyReducer from "../redux/history/reducer";
import userReducer from "../redux/users/reducer";

//tambahkan reducer masing2 disini
// untuk menampung nama-nama reducernya
export const store = configureStore({
  reducer: {
    start: startReducer,
    games: gamesReducer,
    history: historyReducer,
    auth: authReducer,
    user: userReducer,
  },
});
