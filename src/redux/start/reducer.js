import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { startGameAPI } from "./reducerAPI";

const initialState = {
  dataGame: null,
  isLoading: false,
  error: "",
  success: "",
  status: "idle",
};

export const startGame = createAsyncThunk("start/dataGame", async (payload) => {
  const response = await startGameAPI(payload);
  return response.data
});

export const startSlice = createSlice({
  name: "start",
  initialState,
  reducers: {
    reset: () => initialState
  },
  extraReducers: (builder) => {
    builder
      .addCase(startGame.pending, (state) => {
        state.status = "loading";
      })
      .addCase(startGame.fulfilled, (state, action) => {
        state.status = "idle";
        state.dataGame = action.payload;
      });
  },
});

export const { reset } = startSlice.actions;

export const data = (state) => state.start;

export default startSlice.reducer;
