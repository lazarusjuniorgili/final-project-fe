import axios from "axios";
import BASE_URL from "../../utils/baseUrl";

const token = localStorage.getItem("accessToken");

export const startGameAPI = async (payload) => {
  const res = await axios.post(`${BASE_URL}/game/startgame`, payload, {
    headers: { Authorization: token },
  });

  return res;
};
