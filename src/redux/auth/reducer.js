// NB: MAS ROY
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { loginPostAPI, postRegisterAPI } from "./reducerAPI";

const initialState = {
  message: "",
  isLoading: false,
  isError: false,
  isSuccess: false,
};

export const loginUser = createAsyncThunk(
  "login/data",
  async (payload, thunkAPI) => {
    try {
      const response = await loginPostAPI(payload);
      localStorage.setItem("accessToken", response.data.data.accessToken);
      localStorage.setItem("userId", response.data.data.id);
      localStorage.setItem("username", response.data.data.username);
      return response.data.message;
    } catch (error) {
      if (error.response) {
        const message = error.response.data.message;
        return thunkAPI.rejectWithValue(message);
      }
      return thunkAPI.rejectWithValue("server internal error");
    }
  }
);

export const registerUser = createAsyncThunk(
  "register/data",
  async (payload, thunkAPI) => {
    try {
      const response = await postRegisterAPI(payload);
      return response.data.message;
    } catch (error) {
      if (error.response) {
        const message = error.response.data.message;
        return thunkAPI.rejectWithValue(message);
      }
      return thunkAPI.rejectWithValue("server internal error");
    }
  }
);

export const postsSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    reset: () => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(registerUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.message = action.payload;
      })
      .addCase(registerUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(loginUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.message = action.payload;
      })
      .addCase(loginUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export const { reset } = postsSlice.actions;

export const data = (state) => state.auth;

export default postsSlice.reducer;
