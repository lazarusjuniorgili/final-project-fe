import axios from "axios";
import BASE_URL from "../../utils/baseUrl";

export const getAllUserAPI = async () => {
  const response = await axios.get(`${BASE_URL}/user`);
  console.log(response);
  return response;
};

export const getUserByIdAPI = async (id) => {
  const response = await axios.get(`${BASE_URL}/user/${id}`);
  return response;
};

export const updateUserAPI = async (payload) => {
  const response = await axios.put(`${BASE_URL}/user/${payload.id}`, payload);
  return response;
};

export const deleteUserAPI = async (id) => {
  const response = await axios.delete(`${BASE_URL}/user/${id}`);
  return response;
};
