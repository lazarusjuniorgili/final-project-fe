import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getHistoryAPI, getAllHistoryAPI } from "./reducerAPI";

const initialState = {
  data: [],
  history: [],
  isLoading: false,
  error: "",
  success: "",
  status: "idle",
};

export const getHistory = createAsyncThunk("history/data", async (userId) => {
  const response = await getHistoryAPI(userId);
  return response.data.data;
});

export const getAllHistory = createAsyncThunk("history/alldata", async () => {
  const response = await getAllHistoryAPI();
  return response.data.data;
});

export const historySlice = createSlice({
  name: "history",
  initialState,
  reducers: {
    cleanHistory(state) {
      state.history = [];
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getHistory.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getHistory.fulfilled, (state, action) => {
        state.status = "idle";
        state.history = action.payload;
      })
      .addCase(getAllHistory.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getAllHistory.fulfilled, (state, action) => {
        state.status = "idle";
        state.data = action.payload;
      });
  },
});

export const data = (state) => state.history;

export const { cleanHistory } = historySlice.actions;

export default historySlice.reducer;
