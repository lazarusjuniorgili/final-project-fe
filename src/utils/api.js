import axios from "axios";

const URL = 'http://localhost:5000'

function getAccessToken() {
  return localStorage.getItem('accessToken');
}

function putAccessToken(accessToken) {
  return localStorage.setItem('accessToken', accessToken);
}

async function fetchWithToken(url) {
  return axios.get(url, {
    headers: {
      'Authorization': getAccessToken(),
    }
  })
}

async function getUserLogged() {
  try {
    const response = await fetchWithToken(`${URL}/whoami`);
    return { error: false, data: response.data };
  } catch (err) {
    if (err.code === 'ERR_NETWORK') return { error: true, message: 'Server not connected!' }
    return { error: true, message: err.message }
  }
}

async function putUserLogged(id, user) {
  try {
    const response = await axios.put(`${URL}/user/update/${id}`, user, {
      headers: {
        'Authorization': getAccessToken()
      }
    })
    return { error: false, message: response.data.status, data: response.data.data }
  } catch (err) {
    if (err.code === 'ERR_NETWORK') return { error: true, message: 'Server not connected!' }
    return { error: true, message: err.response.data.message, data: null }
  }
}

async function getGames() {
  try {
    const response = await axios.get(`${URL}/game/getgame`);
    return { error: false, message: response.data.status, data: response.data.data }
  } catch (err) {
    if (err.code === 'ERR_NETWORK') return { error: true, message: 'Server not connected!' }
    return { error: true, message: 'No game data on server!', data: null }
  }
}

async function getAllHistory() {
  try {
    const response = await axios.get(`${URL}/history/getallhistory`);
    return { error: false, message: response.data.status, data: response.data.data };
  } catch (err) {
    if (err.code === 'ERR_NETWORK') return { error: true, message: 'Server not connected!' }
    return { error: true, message: err.message }
  }
}

export {
  getAccessToken,
  putAccessToken,
  getUserLogged,
  getGames,
  getAllHistory,
  putUserLogged,
} 