import React from "react";
import { Container } from "react-bootstrap";
import Navigation from "../components/Navigation";

function ErrorPage({authedUser}) {
    return (
        <div>
            <Navigation authedUser={authedUser} />
            <Container expand="lg">
                <div className="error-page py-5">
                    <h2>404</h2>
                    <p>Page not found!</p>
                </div>
            </Container>
        </div>
    )
}

export default ErrorPage;