import React, { useEffect, useState } from 'react';
import { getGamesThunkFunc, selectGameList } from '../redux/games/reducer';
import { getHistory, cleanHistory } from '../redux/history/reducer';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from 'react-bootstrap';
import Navigation from '../components/Navigation';
import SearchBar from '../components/SearchBar';
import HeroSection from '../components/HeroSection';
import GridGame from '../components/GridGame';
import Footer from '../components/Footer';
import Loader from '../components/Loader';
import ButtonAdd from '../components/ButtonAdd';
import Toast from '../components/Toast';

function LandingPage() {
  const dispatch = useDispatch();
  const [keyword, setKeyword] = useState('');
  const firstRun = React.useRef(true);
  const games = useSelector(selectGameList);
  const histories = useSelector((state) => state.history.history);
  const { status, error, message } = useSelector((state) => state.games);
  const userId = localStorage.getItem('userId');

  useEffect(() => {
    if (firstRun.current) {
      dispatch(getGamesThunkFunc());
      if (userId) dispatch(getHistory(userId));
      firstRun.current = false;
    }

    return () => {
      dispatch(cleanHistory());
    }
  }, [dispatch]);

  const onSearchHandler = (keyword) => setKeyword(keyword);

  const filteredGames = games
    ?.filter((game) => game.name?.toLowerCase().includes(keyword.toLowerCase()))
    .map((game) => ({
      ...game,
      history: histories.find((data) => data.game_id === game.id),
    }));

  if (status === 'loading') return <Loader />;

  return (
    <>
      <Navigation />
      <Container expand="lg">
        {error && <Toast message={message} />}
        <SearchBar onSearch={onSearchHandler} />
        <HeroSection games={games} />
        <GridGame games={filteredGames} />
      </Container>
      <ButtonAdd />
      <Footer />
    </>
  );
}

export default LandingPage;
