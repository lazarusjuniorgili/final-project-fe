import React from "react";
import { Link } from "react-router-dom";
import { BsFillCaretLeftFill } from "react-icons/bs";
import { Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import AddGameInput from "../components/AddGameInput";
import { useDispatch, useSelector } from "react-redux";
import { addGame, getGamesThunkFunc, selectGameList } from "../redux/games/reducer";

const AddGame = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const games = useSelector(selectGameList)

  const onAddGame = async (payload) => {
    dispatch(addGame(payload))

    navigate("/");
  };

  React.useEffect(() => {
    dispatch(getGamesThunkFunc());
  }, [dispatch]);

  return (
    <Container>
      <Link to="/" className="game-btn-back">
        <BsFillCaretLeftFill />
      </Link>
      <Row style={{ height: "100vh" }}>
        <Col
          style={{ maxWidth: "800px" }}
          className="d-grid align-items-center mx-auto"
        >
          <AddGameInput addGame={onAddGame} games={games} />
        </Col>
      </Row>
    </Container>
  );
};

export default AddGame;
