import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import AppBar from "../components/start/AppBar";
import Start from "../components/start/Start";
import { data, reset, startGame } from "../redux/start/reducer";

const StartPage = () => {
  const [round, setRound] = useState(0);
  const [pointGame, setPointGame] = useState(0);
  const { id } = useParams();
  const { dataGame } = useSelector(data);
  const userId = localStorage.getItem('userId');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const increase = () => setRound((prevValue) => prevValue + 1);
  
  const direct = () => {
    navigate(`/game/${id}`);
  };

  const onClickHandle = (choice) => {
    const payload = {
      user_id: userId,
      game_id: id,
      choice: choice,
    };

    dispatch(startGame(payload))
    .then(({ payload }) => setPointGame((prevValue) => prevValue + payload.point));
    increase();
  };

  useEffect(() => {
    if (round === 3) {
      setTimeout(() => {
        dispatch(reset())
      }, 2000)
    }
  }, [round, dispatch, reset])

  return (
    <>
      <AppBar />
      <Container>
        <Start
          direct={direct}
          handleClick={onClickHandle}
          dataGame={dataGame}
          round={round}
          setRound={setRound}
          pointGame={pointGame}
          setPointGame={setPointGame}
        />
      </Container>
    </>
  );
};

export default StartPage;
