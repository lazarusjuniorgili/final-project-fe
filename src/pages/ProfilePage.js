import React, { useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { BsFillCaretLeftFill } from "react-icons/bs";
import SideleftProfile from "../components/SideleftProfile";
import SiderightProfile from "../components/SiderightProfile";
import ErrorPage from "./ErrorPage";
import { getAccessToken } from "../utils/api";
import { useDispatch, useSelector } from "react-redux";
import { data as dataHistory, getHistory } from "../redux/history/reducer";
import { getUserById, data as dataGetUserId, updateUser } from "../redux/users/reducer";

function ProfilePage() {
  const dispatch = useDispatch();
  const { history } = useSelector(dataHistory);
  const { data } = useSelector(dataGetUserId);
  const userId = localStorage.getItem("userId");
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("userId");
    localStorage.removeItem("username");
    localStorage.removeItem("accessToken");
    navigate("/");
  };

  const fetchData = () => {
    dispatch(getUserById(userId));
    dispatch(getHistory(userId));
  };

  useEffect(() => {
    fetchData();
  }, []);

  const onUpdate = async (payload) => {
    dispatch(updateUser({ id: userId, ...payload }));
  };

  if (getAccessToken() === "") return <ErrorPage />;

  const newData = [...history].sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt));

  return (
    <Container fluid>
      <Row>
        <Link to="/" className="game-btn-back">
          <BsFillCaretLeftFill />
        </Link>
        <SideleftProfile user={data} logout={logout} onUpdate={onUpdate} />
        <SiderightProfile {...data} history={newData} />
      </Row>
    </Container>
  );
}

export default ProfilePage;
