import React from "react";
import {
    Col,
} from "react-bootstrap";
import HistoryGameItem from "./HistoryGameItem";

function SiderightProfile({ total_point, history }) {

    return (
        <Col sm={8} className='sideright-container'>
            <h2 className="p-title-score">Skor mu</h2>
            <p className="p-total-score">{total_point ? total_point : 0}</p>
            <div className="p-container-history">
                <h3 className="p-title-history mb-4">HISTORY GAME</h3>
                {history?.map(data => (
                    <HistoryGameItem key={data?.id} {...data} />
                ))}
            </div>
        </Col>
    )
}

export default SiderightProfile;