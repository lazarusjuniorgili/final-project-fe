import React, { useEffect, useState } from "react";
import { Col, Button, Modal, Form } from "react-bootstrap";

function SideleftProfile({ user, logout, onUpdate }) {
  const [name, setNameInput] = useState("");
  const [username, setUserameInput] = useState("");
  const [email, setEmailInput] = useState("");
  const [showEdit, setShowEdit] = useState(false);

  const handleShowEdit = () => setShowEdit(true);
  const handleCloseEdit = () => setShowEdit(false);
  const onSubmitHandler = () => {
    handleCloseEdit();
    onUpdate({ name, username, email });
  };

  useEffect(() => {
    setNameInput(() => user?.name || "");
    setUserameInput(() => user?.username || "");
    setEmailInput(() => user?.email || "");
  }, [user]);

  return (
    <Col sm={4} className="sideleft-container">
      <div className="profile__item my-4">
        <img src={`https://ui-avatars.com/api/?name=${user?.username}&background=random&size=200`} alt="profile avatar"></img>
      </div>
      <h2>{user?.name}</h2>
      <p className="profile__name">{user?.username}</p>
      <p className="profile__name">{user?.email}</p>
      <Button variant="link" className="game-btn-edit my-4" onClick={handleShowEdit}>
        Edit Data
      </Button>
      <Button variant="link" className="game-btn-logout my-5" onClick={logout}>
        Logout
      </Button>

      <Modal show={showEdit} onHide={handleCloseEdit}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Form</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h2>PLAY MORE, FEEL LIVE MORE</h2>
          <Form>
            <Form.Group className="mb-3">
              <Form.Control className="form-control-profile" type="text" placeholder="name" value={name} onChange={({ target }) => setNameInput(target.value)} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Control className="form-control-profile" type="text" placeholder="username" value={username} onChange={({ target }) => setUserameInput(target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control className="form-control-profile" type="email" placeholder="email" value={email} onChange={({ target }) => setEmailInput(target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" className="btn-modal" onClick={handleCloseEdit}>
            Close
          </Button>
          <Button variant="primary" className="btn-modal" onClick={onSubmitHandler}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </Col>
  );
}

export default SideleftProfile;
