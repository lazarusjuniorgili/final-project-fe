import React from "react";

function HistoryGameItem({ result, game, point }) {
    return (
        <div className="p-history-item">
            <p>{game?.name}</p>
            <p>{result}
                <span> {point !== 0 ? `+${point}` : point}</span>
            </p>
        </div>
    )
}

export default HistoryGameItem;