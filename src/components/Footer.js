import React from "react";
import {
    Navbar,
    Container,
    Nav,

} from "react-bootstrap";
import { Link } from "react-router-dom";


function Footer() {
    return (
        <Navbar bg="light" expand="lg" className="mt-5">
            <Container expand="lg">
                <Navbar.Brand className="me-5">
                    <Link to="/" className="nav-link">
                        <img src="/icon/2.ico" alt="icon" width="50px" height="50px" />
                        .ico
                    </Link>
                </Navbar.Brand>
                <Nav>
                    <Nav.Link href="/">Game Project CH9</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    );
}

export default Footer;