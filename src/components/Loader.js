import React from "react";

function Loader() {
    return (
        <div className="loader-wrapper">
            <div className="lmns lmns-loader three-wavy-balls">
                <span />
                <span />
                <span />
            </div>
        </div>
    );
}

export default Loader;