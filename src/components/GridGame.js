import React from "react";
import { useState } from "react";
import { Container, Row, Button } from "react-bootstrap";
import GridGameItem from './GridGameItem';

function GridGame({ games }) {
  const itemPerRow = 4;
  const [next, setNext] = useState(itemPerRow);

  const handleMoreImage = () => setNext(next + itemPerRow);
  const handleLessImage = () => setNext(next - itemPerRow);

  return (
    <Container className="container-grid-game">
      <h2 className="text-center mb-4">LIST GAME</h2>
      {games?.length === 0 ? (
        <h3 className="my-5 py-5 text-center">Game not found!</h3>
      ) : (
        <Row xs={1} md={3} lg={4} className="g-4">
          {games?.slice(0, next)?.map((game) => (
            <GridGameItem key={game?.id} {...game} />
          ))}
        </Row>
      )}

      <div className="d-flex justify-content-center">
        {itemPerRow < next && (
          <Button
            className="m-4 game-btn-less"
            onClick={handleLessImage}
            variant="link"
          >
            show less
          </Button>
        )}
        {next < games?.length && (
          <Button className="m-4 game-btn-second" onClick={handleMoreImage}>
            more game
          </Button>
        )}
      </div>
    </Container>
  );
}

export default GridGame;
