import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, Button, Col, Badge } from 'react-bootstrap';

function GridGameItem({ id, cover, name, history }) {
  const navigate = useNavigate();

  return (
    <Col>
      <Card className="position-relative text-center">
        {history &&
          <Badge className="position-absolute top-0 start-0 fs-6 mt-3 ms-3 shadow" bg="warning" text="dark">
            Pernah Dimainkan
          </Badge>}
        <Card.Img
          variant="top"
          src={cover?.includes('.jpg') ? `images/${cover}` : 'images/logo.webp'}
        />
        <Card.Body className="game-list">
          <Card.Title>{name}</Card.Title>
          <Button onClick={() => navigate(`/game/${id}`)} className="game-btn-card">
            PLAY NOW
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}

export default GridGameItem;
