import React from "react";
import Table from "react-bootstrap/Table";
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAllUser, data as dataUser } from "../../../redux/users/reducer";

function StripedRowExample() {
  const { detail } = useSelector(dataUser);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllUser());
  }, [dispatch]);

  const sortData = [...detail].sort((a, b) => b.total_point - a.total_point);

  return (
    <Table striped className="">
      <thead>
        <tr>
          <th className="w-25">RANK</th>
          <th className="w-25">USERNAME</th>
          <th className="w-25">NAME</th>
          <th className="w-25">SKOR</th>
        </tr>
      </thead>
      <tbody>
        {sortData.map((row, i) => {
          return (
            <tr key={i}>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {i + 1}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.username}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.name}
              </td>
              <td style={{ color: "#41B07B", fontWeight: 600, fontSize: 16 }}>
                {row.total_point}
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}

export default StripedRowExample;
