import React, { useState } from "react";
import { Form, InputGroup, Button } from "react-bootstrap";
import { BsSearch } from "react-icons/bs";

function SearchBar({ onSearch }) {

    const [keyword, setKeyword] = useState('');

    function onKeywordChangeHandler(event) {
        setKeyword(event.target.value);
    }

    function onSubmitHandler(event) {
        event.preventDefault();
        onSearch(keyword);
    }

    return (
        <>
            <Form onSubmit={onSubmitHandler}>
                <InputGroup className="my-4">
                    <Form.Control
                        placeholder="Search . . ."
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        value={keyword}
                        onChange={onKeywordChangeHandler}
                    />
                    <Button type="submit" variant="outline-secondary" id="button-addon2" className="game-btn-search">
                        <BsSearch />
                    </Button>
                </InputGroup>
            </Form>
        </>
    )
}

export default SearchBar;