import React from "react";
import { Navbar, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { HiHome } from 'react-icons/hi2'
const AppBar = () => {
  return (
    <Navbar>
      <Container style={{ backgroundColor: "#65DBA3" }} className="px-4">
        <Navbar.Brand>
          <img
            alt=""
            src="/images/logo-game.png"
            height="50px"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Link to='/' className="text-uppercase nav-link fw-bold">
          <HiHome size={'1.5rem'} />
        </Link>
      </Container>
    </Navbar>
  );
};

export default AppBar;
