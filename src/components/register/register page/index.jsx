import React from "react";
import { useState, useEffect } from "react";
import Alert from "react-bootstrap/Alert";
import "./style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { registerUser, data, reset } from "../../../redux/auth/reducer";
import Lottie from "lottie-react";
import anims from "../../../assets/animations/24219-controller.json";
const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [messagesRegister, setMessagesRegister] = useState("");
  const { message, isSuccess } = useSelector(data);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const AuthLogin = () => {
    navigate("/login");
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const payload = { username, name, email, password };
    dispatch(registerUser(payload));
  };

  useEffect(() => {
    if (isSuccess) {
      setLoading(false);
      setMessagesRegister(message);
      setTimeout(() => {
        navigate("/login");
        dispatch(reset());
      }, 3000);
    } else {
      setMessagesRegister(message);
      setTimeout(() => {
        dispatch(reset());
      }, 10000);
    }
  });

  return (
    <div className="register-page">
      <div className="reg-container">
        {messagesRegister && (
          <Alert variant="info" className="text-center w-50 mx-auto">
            {message}
          </Alert>
        )}
        {loading && (
          <Alert variant="light" className="text-center w-50 mx-auto">
            Loading
          </Alert>
        )}
        <div className="box-register">
          <form>
            <div className="circle">
              <Lottie animationData={anims}></Lottie>
            </div>
            <input
              placeholder="Name"
              className="name-input"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <input
              placeholder="Username"
              className="username-input"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <div className="input-email-pass">
              <input
                placeholder="Email"
                className="email-input"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <input
                placeholder="Password"
                className="password-input"
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <Button
              variant="light"
              className="register-button"
              type="submit"
              onClick={handleSubmit}
              disabled={!username || !password || !name || !email}
            >
              REGISTER
            </Button>
            <p className="sign-in">
              Already have an account?
              <Button className="btn-sign-in" onClick={AuthLogin}>
                SIGN IN
              </Button>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
